package it.sgsbp.application.commons.GenKey;

public class GenKeyException extends RuntimeException {
	/**
	 * Uid
	 */
	private static final long serialVersionUID = 8629997456492996673L;
	/**
	 * Errore numerico
	 */
	public static final int NUMBER_ERROR = 1;
	/**
	 * Errore lunghezza
	 */
	public static final int PWDLEN_ERROR = 2;
	/**
	 * Errore nel seed
	 */
	public static final int PWDSEED_ERROR = 3;
	/**
	 * Errore di case
	 */
	public static final int PWDCASE_ERROR = 4;
	/**
	 * Codice dell'errore
	 */
	private int code;

	/**
	 * Costruttore
	 */
	public GenKeyException() {
	}
	/**
	 * Eccezione specifica
	 * @param paramInt
	 */
	public GenKeyException(int paramInt) {
		this.code = paramInt;
	}

	/**
	 * Codice dell'eccezione
	 * @return
	 */
	public int getCode() {
		return this.code;
	}
}
