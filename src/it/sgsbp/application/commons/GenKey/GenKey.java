package it.sgsbp.application.commons.GenKey;

/**
 * Classe per la generazione di chiavi
 * 
 * @author es03645
 * 
 */
public class GenKey {
	public static final int MIN_NUMBER_LENGTH = 1;
	public static final int MAX_NUMBER_LENGTH = 999999999;
	public static final int MIN_KEY_LENGTH = 4;
	public static final int MAX_KEY_LENGTH = 16;
	public static final int MIN_KEY_SEED = 10000000;
	public static final int MAX_KEY_SEED = 999999999;
	public static final int CASE_ALL = 0;
	public static final int CASE_UPPER = 1;
	public static final int CASE_LOWER = 2;
	public static final int CASE_ALPHA_UPPER = 3;
	public static final int CASE_ALPHA_LOWER = 4;
	public static final int CASE_DIGIT = 5;
	public static final String DIGIT = "0123456789";
	public static final String ALPHALOWER = "abcdefghijklmnopqrstuvwxyz";
	public static final String ALPHAUPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	//private static final int POSITIONS_SKIP = 7;
	//private static final int POSITIONS_LEN = 6;
	private static final long[] BITMASK_LEN = { 0, 1L, 3L, 7L, 15L, 31L, 63L,
			127L, 255L, 511L, 1023L, 2047L, 4095L, 8191L, 16383L, 32767L,
			65535L, 131071L, 262143L, 524287L, 1048575L, 2097151L, 4194303L,
			8388607L, 16777215L, 33554431L, 67108863L, 134217727L, 268435455L,
			536870911L, 1073741823L, 2147483647L, -1L };
	private static final long[] SPACINGMASK = { -1L, 1431655765L, 1227133513L,
			286331153L, 1108378657L, 136331393L, 270549121L, 269488144L };
	private static final int[] POSITIONSTART = { 23, 13, 6, 17, 5, 31 };
	private String digit;
	private String alphaLower;
	private String alphaUpper;

	/**
	 * Costruttore di default con ammissibilita di numeri 0...9 lettere alfabeto
	 * minuscole a...z lettere alfabeto mqaiuscole A...Z
	 * 
	 */
	public GenKey() {
		this(DIGIT, ALPHALOWER, ALPHAUPPER);
	}

	/**
	 * Realizza un generatore basto su un insieme specifico
	 * 
	 * @param digit
	 * @param alphaLower
	 * @param alphaUpper
	 */
	public GenKey(String digit, String alphaLower, String alphaUpper) {
		this.digit = digit;
		this.alphaLower = alphaLower;
		this.alphaUpper = alphaUpper;
	}

	/**
	 * Gira i bytes
	 * 
	 * @param paramLong
	 * @param paramInt1
	 * @param paramInt2
	 * @param paramInt3
	 * @param paramInt4
	 * @return
	 */
	private static long swapBits(long paramLong, int paramInt1, int paramInt2,
			int paramInt3, int paramInt4) {
		int i = paramInt2 * paramInt3;
		if (i > 32)
			i = 32;
		long l1 = paramLong;
		long l2 = paramLong;
		l1 >>= paramInt1;
		l1 = l1 & BITMASK_LEN[i] & SPACINGMASK[paramInt3];
		l1 <<= paramInt4;
		l2 >>= paramInt4;
		l2 = l2 & BITMASK_LEN[i] & SPACINGMASK[paramInt3];
		l2 <<= paramInt1;
		paramLong = paramLong | l1 | l2;
		return paramLong & (l1 | 0xFFFFFFFF ^ BITMASK_LEN[i])
				& (l2 | 0xFFFFFFFF ^ BITMASK_LEN[i]);
	}

	/**
	 * Effettua un multi scambio dei bit
	 * 
	 * @param paramLong
	 * @return
	 */
	private static long invertValue(long paramLong) {
		paramLong = swapBits(paramLong, 0, 4, 0, 28);
		paramLong = swapBits(paramLong, 4, 4, 0, 24);
		paramLong = swapBits(paramLong, 8, 4, 0, 20);
		paramLong = swapBits(paramLong, 12, 4, 0, 16);
		return paramLong;
	}

	/**
	 * Ritrona una Chiave
	 * 
	 * @param paramLong1
	 * @param paramInt1
	 * @param tipologia 
	 * 0 digit lower upper
	 * 1 digit upper
	 * 2 digit lower
	 * 3 upper
	 * 4 lower
	 * 5 digit
	 * @param paramLong2
	 * @return
	 * @throws GenKeyException
	 */
	public String generateKey(long paramLong1, int paramInt1, int tipologia,
			long paramLong2) throws GenKeyException {
		if ((paramLong1 < 1L) || (paramLong1 > 999999999L))
			throw new GenKeyException(1);
		if ((paramInt1 < 4) || (paramInt1 > 16))
			throw new GenKeyException(2);
		if ((paramLong2 < 10000000L) || (paramLong2 > 999999999L))
			throw new GenKeyException(3);
		StringBuffer localStringBuffer = new StringBuffer();
		int i;
		switch (tipologia) {
		case 0:
			localStringBuffer.append(this.digit);
			localStringBuffer.append(this.alphaLower);
			localStringBuffer.append(this.alphaUpper);
			i = localStringBuffer.length();
			break;
		case 1:
			localStringBuffer.append(this.digit);
			localStringBuffer.append(this.alphaUpper);
			i = localStringBuffer.length();
			break;
		case 2:
			localStringBuffer.append(this.digit);
			localStringBuffer.append(this.alphaLower);
			i = localStringBuffer.length();
			break;
		case 3:
			localStringBuffer.append(this.alphaUpper);
			i = localStringBuffer.length();
			break;
		case 4:
			localStringBuffer.append(this.alphaLower);
			i = localStringBuffer.length();
			break;
		case 5:
			localStringBuffer.append(this.digit);
			i = localStringBuffer.length();
			break;
		default:
			throw new GenKeyException(4);
		}
		
		long l1 = invertValue(paramLong1) + paramLong2 & 0xFFFFFFFF;
		long l2 = 0L;
		int j = 0;
		int n = -3;
		char[] arrayOfChar = new char[paramInt1];
		for (int k = 0; k < paramInt1; k++) {
			if (l2 == 0L) {
				l2 = l1;
				n += 3;
			}
			int m = POSITIONSTART[(k % 6)];
			long l3 = l2 % i;
			l2 /= i;
			j = (int) ((j + m + l3 * 7L + n) % i);
			arrayOfChar[k] = localStringBuffer.charAt(j);
		}
		return new String(arrayOfChar);
	}

	/**
	 * Ritorna il dominio dei numeri
	 * @return
	 */
	public String getDigit() {
		return this.digit;
	}

	/**
	 * Ritorna il dominio dei caratteri minuscoli
	 * @return
	 */
	public String getAlphaLower() {
		return this.alphaLower;
	}

	/**
	 * Ritorna il dominio dei caratteri maiuscoli
	 * @return
	 */
	public String getAlphaUpper() {
		return this.alphaUpper;
	}
}