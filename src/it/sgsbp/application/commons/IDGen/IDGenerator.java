package it.sgsbp.application.commons.IDGen;

import it.sgsbp.application.commons.GenKey.GenKey;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Classe per la generazione di ID univoci
 * 
 * @author es03645
 * 
 */
public class IDGenerator {
	private static SecureRandom secureRandom;
	static {
		try {
			secureRandom = SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException localNoSuchAlgorithmException) {
			secureRandom = new SecureRandom();
		}
	}
	
	
	private static long seed = getRandom(10000000L, 999999999L);
	private static long number = getRandom(1L, 249999999L);

	

	/**
	 * Genera un ID univoco
	 * 
	 * @return
	 */
	public static String getID() {
		return getID(16, 0, seed);
	}

	/**
	 * Genera un id univoco
	 * 
	 * @see GenKey.generateKey
	 * @param paramInt1
	 * @param paramInt2
	 * @param paramLong
	 * @return
	 */
	public static String getID(int paramInt1, int paramInt2, long paramLong) {
		long l = ++number;
		return new GenKey().generateKey(l, paramInt1, paramInt2, paramLong);
	}

	/**
	 * Ritorna il seed
	 * 
	 * @return
	 */
	public static long getSeed() {
		return seed;
	}

	/**
	 * Ritorna un valore random
	 * 
	 * @return
	 */
	public static double getRandom() {
		byte[] arrayOfByte = new byte[4];
		int i = 0;
		secureRandom.nextBytes(arrayOfByte);
		for (int j = 0; j < 4; j++)
			i = (i << 8) + (arrayOfByte[j] & 0xFF);
		return Math.abs(i) / 2147483647.0D;
	}

	/**
	 * Ritorna un valore random
	 * 
	 * @param paramLong1
	 * @param paramLong2
	 * @return
	 */
	public static long getRandom(long paramLong1, long paramLong2) {
		return Math.round(paramLong1 + getRandom() * (paramLong2 - paramLong1));
	}
}