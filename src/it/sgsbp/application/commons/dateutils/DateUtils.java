package it.sgsbp.application.commons.dateutils;

import hirondelle.date4j.DateTime;

import java.util.Calendar;
import java.util.TimeZone;

public class DateUtils {

    public static String getCurrentTimeStamp(String str) {

        // recupero il curren time zone
        TimeZone timeZone = Calendar.getInstance().getTimeZone();
        // logger.debug(timeZone);
        // ottengo la data con il timezone di dove è eseguito l'applicativo
        DateTime now = DateTime.now(timeZone);
        TimeZone timeZoneDefault = TimeZone.getTimeZone("Europe/Rome");
        // ottengo la data con il timezone di dove è eseguito l'applicativo
        DateTime newDt = now.changeTimeZone(timeZone, timeZoneDefault);
        // formatto per il filename
        str = newDt.format(str);
        // logger.debug(str);
        return str;

    }
}
