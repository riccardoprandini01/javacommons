package it.sgsbp.application.commons.fileUtils;

import it.sgsbp.application.commons.dateutils.DateUtils;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;

public class TrivialFileUtils {

    public TrivialFileUtils() {
    }
    public static final String CURRENTDATETIME = "CURRENTDATETIME";

    /**
     * Controlla se un file esiste
     *
     * @param filePath
     * @return
     */
    public static boolean fileExist(String filePath) {
        File f = new File(filePath);
        return (f.exists() && !f.isDirectory());
    }

    public static boolean fileExist(File f) {
        return (f.exists() && !f.isDirectory());
    }

    /**
     *
     * @param path
     * @param pre
     * @param filename
     * @param post
     * @param ext
     * @param separator
     * @param b
     * @param data
     * @return
     * @throws IOException
     */
    public static File saveFile(String path, String pre, String filename,
            String post, String ext, String separator, boolean b, byte[] data)
            throws IOException {

        if (StringUtils.isNotBlank(separator) && !TrivialFileUtils.isValidName(separator)) {
            separator = "_";
        }

        if (!TrivialFileUtils.validateFileName(filename)) {
            filename = getValidFileName(filename);
        }


        pre = prePostChunk(pre);
        post = prePostChunk(post);

        File f = new File(path + File.separator + pre + separator + filename
                + separator + post + ext);

        FileUtils.writeByteArrayToFile(f, data);
        return f;

    }

    /**
     *
     * @param str
     * @return
     */
    private static String prePostChunk(String str) {

        if (StringUtils.equalsIgnoreCase(str, CURRENTDATETIME)) {
            str = DateUtils.getCurrentTimeStamp("YYYYMMDDhhmmssfff");
        }
        return str;
    }

    /**
     *
     * @param text
     * @return
     */
    public static boolean isValidName(String text) {
        Pattern pattern = Pattern
                .compile(
                "# Match a valid Windows filename (unspecified file system).          \n"
                + "^                                # Anchor to start of string.        \n"
                + "(?!                              # Assert filename is not: CON, PRN, \n"
                + "  (?:                            # AUX, NUL, COM1, COM2, COM3, COM4, \n"
                + "    CON|PRN|AUX|NUL|             # COM5, COM6, COM7, COM8, COM9,     \n"
                + "    COM[1-9]|LPT[1-9]            # LPT1, LPT2, LPT3, LPT4, LPT5,     \n"
                + "  )                              # LPT6, LPT7, LPT8, and LPT9...     \n"
                + "  (?:\\.[^.]*)?                  # followed by optional extension    \n"
                + "  $                              # and end of string                 \n"
                + ")                                # End negative lookahead assertion. \n"
                + "[^<>:\"/\\\\|?*\\x00-\\x1F]*     # Zero or more valid filename chars.\n"
                + "[^<>:\"/\\\\|?*\\x00-\\x1F\\ .]  # Last char is not a space or dot.  \n"
                + "$                                # Anchor to end of string.            ",
                Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE
                | Pattern.COMMENTS);
        Matcher matcher = pattern.matcher(text);
        boolean isMatch = matcher.matches();
        return isMatch;
    }

    /**
     * Crea un file in B64 a partire da quello in ingresso codificando riga per
     * riga il file
     *
     * @param f
     * @param f2
     * @throws IOException
     */
    public static void toB64LineByLIne(File f, File f2) throws IOException {

        LineIterator iterator = FileUtils.lineIterator(f/* , "UTF-8" */);
        try {
            while (iterator.hasNext()) {

                String line = iterator.nextLine();
                byte[] lineB64 = Base64.encodeBase64(line.getBytes());
                FileUtils.writeByteArrayToFile(f2, lineB64, true);

                if (iterator.hasNext()) {
                    FileUtils.writeStringToFile(f2, "\n", true);
                }
            }
        } finally {
            LineIterator.closeQuietly(iterator);
        }
    }

    public static File backupFile(File fin) throws IOException {
        String nowStr = prePostChunk(CURRENTDATETIME);
        File fileDest;
        fileDest = new File(fin.getCanonicalPath() + "." + nowStr + ".bak");
        FileUtils.moveFile(fin, fileDest);
        return fileDest;
    }

    public static File backupFile2(File fin) {
        File f;
        try {
            f = backupFile(fin);
        } catch (IOException e) {
            return null;
        }
        return f;
    }

    public static boolean validateFileName(String fileName) {

        return fileName.matches("^[^.\\\\/:*?\"<>|]?[^\\\\/:*?\"<>|]*")
                && getValidFileName(fileName).length() > 0;
    }

    public static String getValidFileName(String fileName) {
        String newFileName = fileName;
        newFileName = StringUtils.replace(newFileName, "<", "_");
        newFileName = StringUtils.replace(newFileName, ">", "_");
        newFileName = StringUtils.replace(newFileName, ":", "_");
        newFileName = StringUtils.replace(newFileName, "\\", "_");
        newFileName = StringUtils.replace(newFileName, "/", "_");
        newFileName = StringUtils.replace(newFileName, "*", "_");
        newFileName = StringUtils.replace(newFileName, "?", "_");
        newFileName = StringUtils.replace(newFileName, "\"", "_");
        newFileName = StringUtils.replace(newFileName, "|", "_");
        //String newFileName = fileName.replaceAll("^[^.\\\\/:*?\"<>|]?[^\\\\/:*?\"<>|]*", "");
        if (newFileName.length() == 0) {
            throw new IllegalStateException(
                    "File Name " + fileName + " results in a empty fileName!");
        }
        return newFileName;
    }
}
