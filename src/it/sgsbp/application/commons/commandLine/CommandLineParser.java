package it.sgsbp.application.commons.commandLine;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

/**
 * Classe per l'analisi della riga di comando.
 * 
 * <pre>
 * CommandLineParser clp = new CommandLineParser();
 * 
 * try {
 *     clp.addPositionalMultivalueParam("channel", 0, new String[] {"INTESA", "ENI" }, true);
 *     clp.addPositionalMultivalueParam("modus", 1, new String[] {"NORMAL", "NORMAL_WITH_TRACEFILE", "RECOVERY","RECOVERY_WITH_TRACEFILE" }, false);
 * } catch (CommandLineParserException e) {
 *     logger.error(e.toString());
 * }
 * 
 * try {
 *     clp.parse(args);
 * } catch (CommandLineParserException e) {
 *     logger.error(e.toString());
 * }
 * 
 * try {
 *     String canale=clp.getValue("channel");
 *     String modo=clp.getValue("modus");
 * 
 * ...
 * </pre>
 * 
 * @author Riccardo Prandini
 * @version 1.0
 * 
 */
public class CommandLineParser {

    LinkedHashMap<String, Command> commandList = new LinkedHashMap<>();

    /**
     * Aggiunge un comando che assuma un multivalore
     *
     * @param name della variabile associata
     * @param position posizione richiesta >=0
     * @param values elenco di valori ammissibili
     * @param needed se deve essere obbligatoriamente immesso
     * @return se vi sono errori
     * @throws CommandLineParserException
     */
    public boolean addPositionalMultivalueParam(String name, int position,
            String[] values, boolean needed) throws CommandLineParserException {

        // Controllo che la posizione sia sensata
        if (position < 0) {
            return false;
        }

        Command c = new Command(position, needed, values);

        // controllo che non conflicgga coi comandi gia immessi
        Iterator<Entry<String, Command>> it = commandList.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, Command> pairs = it.next();
            // nome della variabile associata al comando
            String paramName = pairs.getKey();
            // Comando
            Command command = pairs.getValue();

            // Controllo che la variabile non sia già presente
            if (StringUtils.equalsIgnoreCase(paramName, name)) {
                throw new CommandLineParserException("Comando " + paramName + " Duplicato");
            }

            // Controllo che la posizione non sia già stata assegnata
            if (command.getPosition() == position) {
                throw new CommandLineParserException("Posizione " + position + " già assegnata");
            }

            // it.remove(); // avoids a ConcurrentModificationException
        }

        // Aggiungo il comando
        commandList.put(name, c);
        return true;

    }

    /**
     * Analizza la riga di comando e parserizza
     *
     * @param args
     * @throws CommandLineParserException
     */
    public boolean parse(String[] args) throws CommandLineParserException {

        // prendo tutti gli argomenti passati dalla linea di comando
        for (int i = 0; i < args.length; i++) {

            int posizione = i;
            // scorro i comandi per vedere se la posizione è di qualche comando

            Iterator<Entry<String, Command>> it = commandList.entrySet()
                    .iterator();
            boolean setted = false;
            while (it.hasNext()) {
                Entry<String, Command> pairs = it.next();

                // Comando in esame
                Command command = pairs.getValue();

                // Se la posizione collima
                if (command.getPosition() == posizione) {
                    // Salvo il valore del comando
                    command.setAssignedValue(args[i]);
                    setted = true;
                    break;
                }

            }
            if (!setted) {
                throw new CommandLineParserException("Comando non parserizzato");
            }

        }

        // Controllo se i comandi obbligatori sono stati impostati

        Iterator<Entry<String, Command>> it = commandList.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, Command> pairs = it.next();

            String paramName = pairs.getKey();

            // Comando in esame
            Command command = pairs.getValue();

            // Se è necessario e non è assegnato
            if (command.isNeeded()
                    && StringUtils.isBlank(command.getAssignedValue())) {
                throw new CommandLineParserException("Il parametro "
                        + paramName + " è obbligatorio");
            }
        }
        return true;


    }

    /**
     * Classe interna per la gestione delle informazioni legate ad un comando
     *
     * @author Riccardo Prandini
     *
     *
     */
    private class Command {

        public boolean getAssignedValue;

        /**
         * Costruttore per assegnare le informazioni ad un comando
         *
         * @param position posizione del comando
         * @param needed indica se è obbligatoria la sua impostazione
         * @param values valori ammessi dal comando
         */
        public Command(int position, boolean needed, String[] values) {
            this.position = position;
            this.needed = needed;
            this.values = (String[]) ArrayUtils.clone(values);
        }

        public boolean isNeeded() {
            return this.needed;
        }

        public String getAssignedValue() {
            return this.assignedValue;
        }

        public int getPosition() {
            return this.position;
        }

        public boolean setAssignedValue(String assignedValue)
                throws CommandLineParserException {
            if (ArrayUtils.contains(values, assignedValue)) {
                this.assignedValue = assignedValue;
                return true;
            } else {
                throw new CommandLineParserException("Valore " + assignedValue
                        + " errato previsti:" + StringUtils.join(values, ","));
            }
        }
        private String[] values;
        //private String command;
        private int position;
        private String assignedValue;
        private boolean needed;
    }

    /**
     * Classe per le eccezioni
     *
     * @author Riccardo Prandini
     *
     */
    public class CommandLineParserException extends Exception {

        private static final long serialVersionUID = 8289910236643310115L;

        public CommandLineParserException(String message) {
            super(message);
        }
    }

    public String getValue(String string) throws CommandLineParserException {
        if (!commandList.containsKey(string)) {
            throw new CommandLineParserException("Impossibile trovare il comando richiesto: " + string);
        } else {
            return commandList.get(string).assignedValue;

        }

    }
}
