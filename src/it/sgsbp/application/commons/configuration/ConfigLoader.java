package it.sgsbp.application.commons.configuration;

import it.sgsbp.application.commons.fileUtils.TrivialFileUtils;
import java.io.File;
import java.io.IOException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
/*
 * Classe utilizzata per il caricamento delle impostazioni dell'applicazione
 * <pre>
 * try {
 *      ConfigLoader.loadConfiguration();
 * } catch (ConfigLoaderException e1) {
 *      System.exit(AppReturnValue.ERROR_EXIT_CODE.getReturnCode());
 * }
 * 
 * String uniqueLoggerName=null;
 * try {
 *      uniqueLoggerName=ConfigLoader.getProperties("LOGGER_NAME");
 * } catch (ConfigLoaderException e1) {
 *      System.exit(AppReturnValue.ERROR_EXIT_CODE.getReturnCode());
 * }
 * 
 * String MAIL_SERVER = ConfigLoader.getProperties("MAIL_SERVER");
 * ConfigLoader.getProperties("INTESA","PATH_REPOSITORY_OUT");
 * </pre>
 */
public class ConfigLoader {

	/**
	 * Definisce il nome del file di configurazione di default con le risorse
	 * dell'applicazione a "d:\\config\\application.xml"
	 */
	public final static String DEFAULT_CONFIGURATION_FILE = "d:\\config\\application.xml";

	/**
	 * Definisce il nome del file di configurazione di default con i logs
	 * dell'applicazione a "d:\\config\\log4j\\log4jApplication.properties"
	 */
	public final static String DEFAULT_LOG_CONFIGURATION_FILE = "d:\\config\\log4j\\log4jApplication.properties";

	private static String logConfig;
	private static String appConfig;

	
	static HashMap<String, String> val= new HashMap<>();
	
	/**
	 * Avvia il caricamento delle configurazioni 
	 * al termine è posibile ottenere
         * 
         * uilizza 
         * - appl.config
         * - log4j.config
         * 
	 * 
	 * @throws ConfigurationException
	 */
	public static void loadConfiguration() throws ConfigLoaderException {
		// Recupero le impostazioni dell'applicazione passate a linea di comando
		appConfig = getCheckedSysPropertiesFileExist("appl.config",DEFAULT_CONFIGURATION_FILE);
		logConfig = getCheckedSysPropertiesFileExist("log4j.config",DEFAULT_LOG_CONFIGURATION_FILE);
		
                //Inizializzo il logger
		loadLogger(logConfig);
		
		//Carico le impostazioni
		loadAppConf(appConfig);
		

	}
	
	/**
	 * Carica le informazioni dal file di configurazion
	 * @param appConfig2
	 * @throws ConfigurationException 
	 */
	private static void loadAppConf(String appConfig2) throws ConfigLoaderException {
		try {
			
		
			
			val.clear();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true); // never forget this!
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(appConfig2);
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr1 = xpath.compile("/environments/environment");
			XPathExpression expr2 = xpath.compile("@key");
			XPathExpression expr12 = xpath.compile("variable");
			XPathExpression expr121 = xpath.compile("@name");
			XPathExpression expr122 = xpath.compile("@value");
		
			NodeList environmentNL = (NodeList) expr1.evaluate(doc, XPathConstants.NODESET);
			for (int i = 0; i < environmentNL.getLength(); i++) {
				Node environmentI = environmentNL.item(i);
				String envKey =  (String) expr2.evaluate(environmentI, XPathConstants.STRING);
				
				
				NodeList variableNL = (NodeList) expr12.evaluate(environmentI, XPathConstants.NODESET);
				for (int j = 0; j < variableNL.getLength(); j++) {
					Node variableI = variableNL.item(j);
					String valueName =  (String) expr121.evaluate(variableI, XPathConstants.STRING);
					String valueValue =  (String) expr122.evaluate(variableI, XPathConstants.STRING);
					
					val.put(envKey+"."+valueName, valueValue);
					
				}
			}
			
			
			
			
		
		
		
		
		} catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
			throw new ConfigLoaderException(e.toString());
			
		}
		
	}

	/**
	 * Carica la configurazione del logger
	 * 
	 * @param logConfig2
	 */
	private static void loadLogger(String logConfig2) {
		org.apache.log4j.LogManager.resetConfiguration( );
		PropertyConfigurator.configure(ConfigLoader.getLoggerConfigPath());
	}

	/**
	 * Controlla che il file indicato dalla proprietà di sistema passata sia un file esistente
	 * se viene passata una stringa valida essa sarà tornata in caso di problemi, nel caso
	 * in cui la stringa di fallback non sia valida è ritornata una eccezione.
	 * 
	 *  
	 *  
	 * @param property proprietà di sistema utilizzata per passare l'elemento
	 * @param default_configuration_file2 file di default nel caso nessuna configurazione fosse impostata 
	 * @return
	 * @throws ConfigurationException
	 */
	private static String getCheckedSysPropertiesFileExist(String property, String default_configuration_file)
			throws ConfigLoaderException {
		String applConfig = System.getProperty(property);
		// Se vuoto lancio una eccezione
		if (applConfig == null) {
			if(StringUtils.isNotBlank(default_configuration_file)){
				return default_configuration_file;
			}else{
				throw new ConfigLoaderException(property+ " in command line e' nullo");
			}
		}
		try {
			// Carico il file
			applConfig = new URL(applConfig).getFile();
		} catch (MalformedURLException e) {
			if(StringUtils.isNotBlank(default_configuration_file)){
				return default_configuration_file;
			}else{
				throw new ConfigLoaderException(applConfig	+ " non e' un uri file corretto");
			}
		}
		// Utility File per controllo esistenza del file
		if (!TrivialFileUtils.fileExist(applConfig)) {
			if(StringUtils.isNotBlank(default_configuration_file)){
				return default_configuration_file;
			}else{
                            File f = new File(applConfig);	
                            try {
                                throw new ConfigLoaderException("Il file " +  applConfig +
                                        "["+f.getAbsolutePath()+"]" +
                                        "["+f.getCanonicalPath()+"]" +
                                        " non esiste o non e' un file");
                            } catch (IOException ex) {
                                 throw new ConfigLoaderException(ex.getMessage());
                            }
			}
		}
		return applConfig;
	}
	
	/**
	 * Ritorna il path del file di configurazione di log4j
	 * 
	 * @return
	 */
	public static String getLoggerConfigPath() {
		return logConfig;
	}
	
	/**
	 * Ritorna il valore della proprietà prendendolo da DEFAULT
	 * 
	 * @param string
	 * @return
	 * @throws ConfigLoaderException 
	 */
	public static String getProperties(String value) throws ConfigLoaderException {
		String namespace="DEFAULT";
		return getProperties(namespace,value);
	}

	/**
	 * Ritorna il valore di una proprietà 
	 * 
	 * @param namespace
	 * @param value
	 * @return
	 * @throws ConfigLoaderException 
	 */
	public static String getProperties(String namespace, String value) throws ConfigLoaderException {
		String param=namespace+"."+value;
		
		if(!val.containsKey(param)){
			throw new ConfigLoaderException(param+" non presente");
		}else{
			return val.get(param);
			
		}
	}
}
