package it.sgsbp.application.commons.configuration;

public class ConfigLoaderException extends Exception{
	
	private static final long serialVersionUID = 5675819095484128841L;

	public ConfigLoaderException(String message) {
		super(message);
	}
}

